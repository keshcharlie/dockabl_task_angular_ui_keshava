import { Component, OnInit, OnDestroy } from '@angular/core';

import { GlobalService } from '../services/global-service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  usersGlobalInfoOn: any;
  userGloabalData: any = {};
  constructor(private globalService: GlobalService) { }

  ngOnInit() {

    this.usersGlobalInfoOn = this.globalService.getUsersGlobalInfoUpdate().subscribe((data: any) => {
      this.userGloabalData = data;
    })

  }

  ngOnDestroy() {
    this.usersGlobalInfoOn.unsubscribe();
  }

}
