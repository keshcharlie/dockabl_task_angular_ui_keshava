import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  template: `
    <div class="message-position">
    <div class="card text-center">
  <div class="card-header">
  </div>
  <div class="card-body">
    <h5 class="card-title">404 page-not-found :-(</h5>
    <p class="card-text">This page will be available soooooooooooon :-) </p>
  </div>
  <div class="card-footer text-muted">
  </div>
</div>
     
    </div>
  `,
  styles: []
})
export class PageNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
