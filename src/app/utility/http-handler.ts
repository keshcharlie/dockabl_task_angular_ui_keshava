import { throwError } from 'rxjs';

export class HttpHandler {

  static extractData(res: Response) {
    let body = res.json();
    return body || {};
  }
  //Http calls error handler
  static handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

}
