import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Routes } from '@angular/router';

import { UsersService } from '../../services/users.service';
import { GlobalService } from '../../services/global-service';


@Component({
  selector: 'app-user-posts',
  templateUrl: './user-posts.component.html',
  styleUrls: ['./user-posts.component.scss']
})
export class UserPostsComponent implements OnInit, OnDestroy {
  queryParamMapOn: any;
  userId: any;
  userPostsList: any = [];
  userPostsValidationMap: any = { status: 'spinner', message: '' };
  activePanelId: string;

  constructor(private route: ActivatedRoute, private usersService: UsersService, private globalService: GlobalService) {
    this.globalService.setUsersGlobalInfoUpdate({ page: 'usersPosts', backButton: true, headerTitle: 'Users Posts' })
  }

  ngOnInit() {
    this.queryParamMapOn = this.route.queryParamMap.subscribe(queryParams => {
      this.userId = queryParams.get("userId");
      if (this.userId !== undefined && this.userId !== null) {
        this.getUserPostsList()
      } else {
        this.userPostsValidationMap.status = "failure";
        this.userPostsValidationMap.message = "Ooops!! Something went wrong.";
      }
    })
  }

  // Fetches the users list 
  private getUserPostsList() {
    this.usersService.getUserPosts(this.userId).subscribe(
      (response) => {
        try {
          this.userPostsList = response;
          if (this.userPostsList.length > 0) {
            this.userPostsValidationMap.status = "success";
          } else {
            this.userPostsValidationMap.status = "failure";
            this.userPostsValidationMap.message = "No users to Display";
          }
        }
        catch (ex) {
          this.userPostsValidationMap.status = "failure";
          this.userPostsValidationMap.message = "Ooops!! Something went wrong.";
        }
      },
      (error) => {
        this.userPostsValidationMap.status = "failure";
        this.userPostsValidationMap.message = "Ooops!! Something went wrong.";
      }
    )
  };

  ngOnDestroy() {
    this.queryParamMapOn.unsubscribe();
  }

}
