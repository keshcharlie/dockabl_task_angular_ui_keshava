import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { UserPostsComponent } from './user-posts/user-posts.component';



const routes: Routes = [
  { path: '', component: UsersComponent },
  { path: 'users-posts', component: UserPostsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
