import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';


import { UsersComponent } from './users.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserPostsComponent } from './user-posts/user-posts.component';

import { UsersService } from '../services/users.service';


@NgModule({
  declarations: [UsersComponent, UserDetailsComponent, UserPostsComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    NgbModule,
    FormsModule
  ],
  entryComponents: [UserDetailsComponent],
  providers: [UsersService]
})
export class UsersModule { }
