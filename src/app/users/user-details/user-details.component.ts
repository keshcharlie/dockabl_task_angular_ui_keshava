import { Component, OnInit, Input } from '@angular/core';

import { UsersService } from '../../services/users.service';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
  userDetails: any = {};
  userDetailsValidationMap: any = { status: 'spinner', message: '' };
  modalHeight: number = window.innerHeight;
  @Input() public userId: number;
  constructor(private usersService: UsersService, public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.getUsersDetails();
  }

  // Fetches the users details 
  private getUsersDetails() {
    this.usersService.getUsersDetails(this.userId).subscribe(
      (response) => {
        try {
          this.userDetails = response;
          if (Object.keys(this.userDetails).length > 0) {
            this.userDetailsValidationMap.status = "success";
          } else {
            this.userDetailsValidationMap.status = "failure";
            this.userDetailsValidationMap.message = "No users to Display";
          }
        }
        catch (ex) {
          this.userDetailsValidationMap.status = "failure";
          this.userDetailsValidationMap.message = "Ooops!! Something went wrong.";
        }
      },
      (error) => {
        this.userDetailsValidationMap.status = "failure";
        this.userDetailsValidationMap.message = "Ooops!! Something went wrong.";
      }
    )
  };
}
