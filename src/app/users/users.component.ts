import { Component, OnInit } from '@angular/core';

import { UsersService } from '../services/users.service';
import { GlobalService } from '../services/global-service';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { UserDetailsComponent } from './user-details/user-details.component';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  usersValidationMap: any = { status: 'spinner', message: '', loadMoreDisable: true };
  loadMoreOptions: any = {
    start: 0,
    limit: 3,
    totalUserCount: 10
  }
  usersList: any = [];
  constructor(private usersService: UsersService, private globalService: GlobalService, private modal: NgbModal) {
    this.globalService.setUsersGlobalInfoUpdate({ page: 'usersList', backButton: false, headerTitle: 'Users List' })
  }

  ngOnInit() {
    this.getUsersList();
  }


  // Fetches the users list 
  private getUsersList() {
    this.usersService.getUsersList(this.loadMoreOptions).subscribe(
      (response: any) => {
        try {
          if (response.length > 0) {
            for (var i = 0; i < response.length; i++) {
              this.usersList.push(response[i]);
            }
            this.usersValidationMap.loadMoreDisable = false;
            this.usersValidationMap.status = "success";
          } else {
            if (this.usersList.length == 0) {
              this.usersValidationMap.status = "failure";
              this.usersValidationMap.message = "No users to Display";
            }
          }
        }
        catch (ex) {
          this.usersValidationMap.status = "failure";
          this.usersValidationMap.message = "Ooops!! Something went wrong.";
        }
      },
      (error) => {
        this.usersValidationMap.status = "failure";
        this.usersValidationMap.message = "Ooops!! Something went wrong.";
      }
    )
  };

  //Opens users detials modal
  public viewUserDetails(userId: number) {
    const modalRef = this.modal.open(UserDetailsComponent, { windowClass: "userDetailsModalClass" });
    modalRef.componentInstance.userId = userId;

  };

  //Load more pagination
  public Loadmore() {
    this.usersValidationMap.loadMoreDisable = true;
    this.loadMoreOptions.start = this.loadMoreOptions.start + this.loadMoreOptions.limit;
    this.getUsersList();
  }


}
