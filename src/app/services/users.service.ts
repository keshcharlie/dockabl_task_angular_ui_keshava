import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';

import { globals } from '../utility/global.constants';

import { Observable } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';

import { HttpHandler } from '../utility/http-handler';


@Injectable()
export class UsersService {

  constructor(private http: HttpClient) { }

  getUsersList(paginatonOptions: any): Observable<any> {
    const params = new HttpParams().set('_start', paginatonOptions.start).set('_limit', paginatonOptions.limit);
    return this.http.get(globals.apiUrl, { params }).pipe(
      retry(1),
      catchError(HttpHandler.handleError));
  }

  getUsersDetails(userId: number): Observable<any> {
    return this.http.get(globals.apiUrl + '/' + userId).pipe(
      retry(1),
      catchError(HttpHandler.handleError));
  }

  getUserPosts(userId: number): Observable<any> {
    return this.http.get(globals.apiUrl + '/' + userId + '/posts').pipe(
      retry(1),
      catchError(HttpHandler.handleError));
  }

}
