import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor() { }

  //Update global info
  private usersGlobalInfo: Subject<any> = new Subject<any>();

  setUsersGlobalInfoUpdate(data: any): void {
    this.usersGlobalInfo.next(data);
  }

  getUsersGlobalInfoUpdate(): Observable<any> {
    return this.usersGlobalInfo.asObservable();

  }



}
